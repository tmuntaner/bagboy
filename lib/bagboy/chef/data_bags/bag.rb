module Bagboy
  module Chef
    module DataBags
      class Bag

        attr_reader :name

        def initialize( opts = {} )
          @path   = opts[:path]   || ''
          @name   = opts[:name]   || ''
          @file   = opts[:file]   || Core::FileHelper.instance
          @scm    = opts[:scm]    || Core::SCMHelper.instance
          @knife  = opts[:knife]  || Bagboy::Chef::Knife.instance
        end

        def items
          get_items @file.get_files( @path )
        end

        def item( name )
          name = name.gsub(/[^\w\-]/, '_')
          path = item_path( name + '.json' )
          Item.new( {path: path, name: name, bag: @name} )
        end

        def create
          pull
          if unique?
            Dir.mkdir @path
            sync
            true
          else
            false
          end
        end

        private

        def sync
          @knife.create_data_bag @name
        end

        def pull
          @scm.pull
        end

        def get_items( items )
          data_items = []
          items.each do |item|
            path = item_path item
            name = item.sub('.json', '')
            data_items << Item.new( {path: path, name: name} )
          end
          data_items
        end

        def item_path ( item )
          File.join( @path, item )
        end

        def unique?
          return false if @name == ''
          return false if @file.get_files(data_bags_path).include?(@name)
          true
        end

        def data_bags_path
          File.expand_path( File.join(@path, '/..'))
        end

      end
    end
  end
end
