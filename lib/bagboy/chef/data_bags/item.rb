require 'json'
require 'digest/sha2'

module Bagboy
  module Chef
    module DataBags
      class Item

        attr_reader :name

        def initialize(opts = {})
          @path = opts[:path] || ''
          @file = opts[:file] || Core::FileHelper.instance
          @name = opts[:name] || ''
          @bag = opts[:bag] || ''
          @scm = opts[:scm] || Core::SCMHelper.instance
          @knife = opts[:knife] || Bagboy::Chef::Knife.instance
          @data = {}
        end

        def data
          if File.exists?(@path.to_s) and @data.empty?
            @data = JSON.parse @file.read(@path)
          else
            @data
          end
        end

        def update (values, fields)
          pull
          text = parse_data(values, fields)
          @file.write @path, text
          sync
          commit
          @data
        end

        def sync
          @knife.update_item(@bag, @name)
        end

        private

        def pull
          @scm.pull
        end

        def commit
          @scm.commit(@path, "Data Bag: #{@bag} | Item: #{@name}")
        end

        def parse_data (values, fields)
          values.try(:each) do |key, value|
            value = clean_value key, value, fields
            if fields[key] == 'password'
              values[key] = create_password value
            else
              values[key] = value
            end
          end
          @data = values.to_h
          JSON.pretty_generate(@data)
        end

        def clean_value(key, value, fields)
          if fields[key] == 'text' or fields[key] == 'password'
            strip value
          elsif fields[key] == 'array'
            value.reject { |c| c.to_s.empty? }
          else
            value
          end
        end

        def strip(string)
          tmp = string.strip!
          (tmp != nil) ? tmp : string
        end

        def create_password (password)
          if password.to_s.empty?
            clean_password
          else
            hash_password password
          end
        end

        def clean_password
          data['password'].to_s.tr("\n", '')
        end

        def hash_password password
          cmd = "openssl passwd -1 '#{password}'"
          `#{cmd}`.tr("\n", "")
        end

      end
    end
  end
end
