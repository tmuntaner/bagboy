require_relative 'data_bags/bag'
require_relative 'data_bags/item'

module Bagboy
  module Chef
    class DataBagsHelper

      include Singleton

      def initialize
        @file = Core::FileHelper.instance
      end

      def setup( opts )
        @file = opts[:file] if opts[:file]
      end

      def data_bag ( bag_name )
        DataBags::Bag.new( {path: data_bag_directory( bag_name ), name: bag_name} )
      end

      def data_bags
        get_data_bags data_bags_directory
      end

      def data_bags_directory
        File.join( Bagboy.chef_repo, 'data_bags' )
      end

      def data_bag_directory( bag )
        File.join( Bagboy.chef_repo, 'data_bags', bag )
      end

      private

      def get_data_bags( path )
        files = @file.get_directories( path )
        bags  = []
        files.each do |file|
          bag = data_bag( file )
          bags << bag
        end
        bags
      end

    end
  end
end
