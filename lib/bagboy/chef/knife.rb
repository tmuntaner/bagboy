module Bagboy
  module Chef
    class Knife

      include Singleton

      def setup( opts={} )
        
      end

      def update_item ( data_bag, item )
        execute_command knife_command('data bag', "from file #{data_bag} #{item}.json")
      end

      def create_data_bag ( data_bag )
        execute_command knife_command('data bag', "create #{data_bag}")
      end

      def knife_command ( command, subcommand )
        "cd #{Bagboy.chef_repo}; knife #{command} #{key_config.to_s}#{config_file_config.to_s}#{subcommand}"
      end

      private

      def key_config
         "-k #{Bagboy.key_path} " if Bagboy.key_path != ''
      end

      def config_file_config
        "-c #{Bagboy.config_path} " if Bagboy.config_path != ''
      end

      def execute_command cmd
        `#{cmd}`
      end

    end
  end
end
