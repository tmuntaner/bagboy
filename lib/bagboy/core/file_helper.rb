module Bagboy
  module Core
    class FileHelper

      include Singleton

      def get_directories ( path)
        Dir.entries(path).select { |entry| File.directory?(File.join(path, entry)) and !(entry == '.' || entry == '..')}.sort
      end

      def get_files( path )
        Dir.entries(path).select { |entry| !(entry == '.' || entry == '..')}.sort
      end

      def read( path )
        File.read path
      end

      def write ( path, text )
        File.open(path, 'w') { |file| file.write(text) }
      end

    end
  end
end