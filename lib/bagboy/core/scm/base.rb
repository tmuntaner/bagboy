module Bagboy
  module Core
    module SCM
      class Base

        def pull
          raise NotImplementedError
        end

        def commit( file, message )
          raise NotImplementedError
        end

      end
    end
  end
end