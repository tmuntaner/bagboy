module Bagboy
  module Core
    module SCM
      class None < Base

        include Singleton

        def pull
          true
        end

        def commit( file, message )
          true
        end

      end
    end
  end
end