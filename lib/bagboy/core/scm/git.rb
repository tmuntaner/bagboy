require 'pathname'

module Bagboy
  module Core
    module SCM
      class Git < Base

        include Singleton

        def pull
          execute_command "cd #{Bagboy.chef_repo}; git pull"
        end

        def commit( file, message )
          execute_command "cd #{Bagboy.chef_repo}; git add '#{clean_filename file}'; git commit '#{clean_filename file}' -m '#{message}'; git push"
        end

        private

        def execute_command cmd
          `#{cmd}`
        end
        
        def clean_filename( file )
          absolute  = Pathname.new(file)
          root      = Pathname.new(Bagboy.chef_repo)
          relative  = absolute.relative_path_from(root)
        end

      end
    end
  end
end