require_relative 'scm/base'
require_relative 'scm/git'
require_relative 'scm/none'

module Bagboy
  module Core
    class SCMHelper

      include Singleton

      attr_reader :scm

      def setup ( opts = {} )
        @scm = opts[:scm]
      end

      def connect
        if Bagboy.scm == 'git'
          @scm ||= SCM::Git.instance
        else
          @scm ||= SCM::None.instance
        end
      end

      def pull
        connect
        @scm.pull
      end

      def commit( file, message )
        connect
        @scm.commit( file, message )
      end

    end
  end
end