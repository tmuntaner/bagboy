require 'bagboy/engine'
require 'bagboy/core/file_helper'
require 'bagboy/core/scm_helper'
require 'bagboy/chef/knife'
require 'bagboy/chef/data_bags_helper'

module Bagboy
  mattr_accessor :chef_repo, :site_name, :password_hash, :scm, :key_path, :config_path

  self.chef_repo      = ''
  self.site_name      = 'Bagboy'
  self.password_hash  = ''
  self.scm            = 'none'
  self.key_path       = ''
  self.config_path    = ''

  def self.config(&block)
    yield(self)
  end

end
