Bagboy::Engine.routes.draw do

    root to: 'pages#index'

    get   '/update',    to: 'pages#update', as: 'update'

    get   '/signin',    to: 'sessions#new',     as: 'bagboy_signin'
    post  '/signin',    to: 'sessions#create'
    get   '/signout',   to: 'sessions#destroy', as: 'signout'

    get   '/signup', to: 'users#new', as: 'bagboy_signup'
    post  '/signup', to: 'users#create'

    get   '/users',             to: 'users#index',  as: 'users'
    get   '/users/new',         to: 'users#new',    as: 'new_user'
    get   '/users/edit/:id',    to: 'users#edit',   as: 'edit_user'
    post  '/users/edit/:id',    to: 'users#update', as: 'update_user'
    get   '/users/delete/:id',  to: 'users#delete', as: 'delete_user'
    get   '/user/:id',          to: 'users#show',   as: 'user'

    get   '/data_bags/:bag/items/new',          to: 'data_bag_items#new',       as: 'new_data_bag_item'
    post  '/data_bags/:bag/items/new',          to: 'data_bag_items#create',    as: 'create_data_bag_item'
    get   '/data_bags/:bag/items/:item/edit',   to: 'data_bag_items#edit',      as: 'edit_data_bag_item'
    post  '/data_bags/:bag/items/:item/edit',   to: 'data_bag_items#update',    as: 'update_data_bag_item'
    get   '/data_bags/:bag/items/:item',        to: 'data_bag_items#index',     as: 'data_bag_items'

    get   '/data_bags/:bag/template', to: 'data_bag_templates#show',    as: 'data_bag_template'

    post  '/data_bags/:bag/template/item',        to: 'data_bag_template_items#create',   as: 'create_data_bag_template_item'
    post  '/data_bags/:bag/template/item/delete', to: 'data_bag_template_items#delete',   as: 'delete_data_bag_template_item'

    get   '/data_bags',       to: 'data_bags#index',  as: 'data_bags'
    get   '/data_bags/new',   to: 'data_bags#new',    as: 'new_data_bag'
    post  '/data_bags/new',   to: 'data_bags#create', as: 'create_data_bag'
    get   '/data_bags/:bag',  to: 'data_bags#show',   as: 'data_bag'

end
