require_dependency "bagboy/application_controller"

module Bagboy
  class DataBagTemplatesController < ApplicationController

    def show
      @bag      = @bag_helper.data_bag( params[:bag] )
      @template = Template::find_or_create_by({data_bag_name: @bag.name})
      render :show
    end

    def edit
      render :edit
    end
  end
end
