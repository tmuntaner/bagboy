module Bagboy
  class ApplicationController < ActionController::Base
    
    before_action :authorize

    def initialize
      super
      @bag_helper = Bagboy::Chef::DataBagsHelper.instance
      @bags       = @bag_helper.data_bags
      @scm        = Bagboy::Core::SCMHelper.instance
    end

    private

    def current_user
      @current_user ||= User.find(session[:user_id]) if session[:user_id]
    end

    helper_method :current_user

    def authorize
      if !session_whitelist?
        redirect_to bagboy_signin_path unless current_user
      end
    end

    def session_whitelist?
      return true if params[:controller] == 'bagboy/sessions'
      if params[:controller] == 'bagboy/users'
        return true if Bagboy::User.count == 0 and (params[:action] == 'new' or params[:action] == 'create')
      end
      false
    end

  end
end
