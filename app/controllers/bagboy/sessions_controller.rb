require_dependency "bagboy/application_controller"

module Bagboy
  class SessionsController < ApplicationController

    def new
      render 'new'
    end

    def create
      user = Bagboy::User.find_by_email(params[:email])
      if user && user.authenticate(params[:password])
        session[:user_id] = user.id
        redirect_to root_url, flash: {success: 'You have successfuly logged in!'}
      else
        flash.now.alert = 'Invalid email or password'
        render "new"
      end
    end

    def destroy
      session[:user_id] = nil
      redirect_to root_url, flash: { success: 'You have successfuly logged out!'}
    end

  end
end
