require_dependency "bagboy/application_controller"

module Bagboy
  class DataBagsController < ApplicationController

    def index
      render :index
    end

    def show
      @bag    = @bag_helper.data_bag params[:bag]
      @items  = @bag.items
      render :show
    end

    def new
      @bag = Bagboy::Chef::DataBags::Bag.new
      render :new
    end

    def create
      opts          = data_bag_params
      opts[:path]   = @bag_helper.data_bag_directory( opts[:name] )
      @bag          = Bagboy::Chef::DataBags::Bag.new( opts )

      if @bag.create
        redirect_to data_bags_path
      else
        render :new
      end
    end

    private

    def data_bag_params
      params.require(:data_bag).permit(:name, :path)
    end

  end
end
