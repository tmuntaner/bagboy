require_dependency "bagboy/application_controller"

module Bagboy
  class PagesController < ApplicationController

    def index
      render :index
    end

    def update
      if @scm.pull
        flash = { success: 'You have successfully updated your chef-repo' }
      else
        flash = { error: 'Something went wrong' }
      end
      redirect_to request.referer, flash: flash
    end

  end
end
