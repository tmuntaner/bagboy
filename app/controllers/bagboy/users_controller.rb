require_dependency "bagboy/application_controller"

module Bagboy
  class UsersController < ApplicationController

    def show
      @user = Bagboy::User.find(params[:id])
      render :show
    end

    def index
      @users = Bagboy::User.all
      render :index
    end

    def new
      @user = Bagboy::User.new
      render :new
    end

    def create
      @user = Bagboy::User.new(user_params)
      if @user.save
        redirect_to users_path, flash: { success: "You have succesfully created #{@user.email}"}
      else
        render 'new'
      end
    end

    def edit
      @user = Bagboy::User.find(params[:id])
      render :edit
    end

    def update
      @user = Bagboy::User.find(params[:id])
      @user.update_attributes( user_params )
      if ( @user.save )
        redirect_to user_path( @user.id ), flash: { success: "You have succesfully updated #{@user.email}!"}
      else
        render :edit
      end
    end

    def delete
      @user = Bagboy::User.find(params[:id])
      @user.delete
      redirect_to users_path, flash: { success: "#{@user.email} successfuly deleted!"}
    end

    private

    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation)
    end

  end
end
