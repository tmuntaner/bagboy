require_dependency "bagboy/application_controller"

module Bagboy
  class DataBagTemplateItemsController < ApplicationController

    def create
      bag       = @bag_helper.data_bag params[:bag]
      template  = Template::find_or_create_by({data_bag_name: bag.name})

      template.item( params[:data_bag_template_item][:name], params[:data_bag_template_item][:type] )

      redirect_to data_bag_template_path( bag.name )
    end

    def delete
      bag       = @bag_helper.data_bag params[:bag]
      template  = Template::find_or_create_by({data_bag_name: bag.name})

      template.delete_item( params[:data_bag_template_item][:name] )

      redirect_to data_bag_template_path( bag.name )
    end

  end
end
