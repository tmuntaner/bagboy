require_dependency "bagboy/application_controller"

module Bagboy
  class DataBagItemsController < ApplicationController

    def index
      @bag  = @bag_helper.data_bag( params[:bag] )
      @item = @bag.item( params[:item] )

      render :index
    end

    def edit
      @bag      = @bag_helper.data_bag( params[:bag] )
      @item     = @bag.item( params[:item] )
      @template = Template::find_or_create_by({data_bag_name: @bag.name})

      render :edit
    end

    def update
      @bag      = @bag_helper.data_bag( params[:bag] )
      @item     = @bag.item( params[:item] )
      @template = Template::find_or_create_by({data_bag_name: @bag.name})

      @item.update params[:data_bag_template_item].permit!, @template.parsed_data

      redirect_to data_bag_items_path({bag: @bag.name, item: @item.name})
    end

    def new
      @bag      = @bag_helper.data_bag( params[:bag] )
      @item     = Chef::DataBags::Item.new
      @template = Template::find_or_create_by({data_bag_name: @bag.name})

      render :new
    end

    def create
      @bag      = @bag_helper.data_bag( params[:bag] )
      @item     = @bag.item params[:data_bag_template_item][:name]
      @template = Template::find_or_create_by({data_bag_name: @bag.name})

      @item.update params[:data_bag_template_item_data].permit!, @template.parsed_data

      redirect_to data_bag_items_path({bag: @bag.name, item: @item.name})
    end

  end
end
