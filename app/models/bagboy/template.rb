module Bagboy
  class Template < ActiveRecord::Base

    before_save :check_id

    def check_id
      data        = parsed_data
      data['id']  = 'text' if data['id'] == nil
      self.data   = data.to_json
    end

    def parsed_data
      self.data ||= '{}'
      JSON.parse( self.data )
    end

    def item ( key, value )
      data                = parsed_data
      data[key.downcase]  = value
      data                = Hash[data.sort_by { |key, value| key }]
      self.data = data.to_json
      save
    end

    def delete_item ( key )
      data = parsed_data
      data.delete key
      self.data = data.to_json
      save
    end

  end
end
