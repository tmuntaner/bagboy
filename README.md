[![Build Status](https://travis-ci.org/rubyrainbows/bagboy.svg?branch=master)](https://travis-ci.org/rubyrainbows/bagboy)
[![Code Climate](https://codeclimate.com/github/rubyrainbows/bagboy.png)](https://codeclimate.com/github/rubyrainbows/bagboy)

# Bagboy

Bagboy is a Rails Mountable Engine created to help manage databags in chef.

**Note:**
*This application assumes that you are storing your chef_repo in git.*

**Warning 1:**
*Use at your own risk. It is a good idea to keep this application away from the general internet to protect it from attacks.*

## Requirements:
* openssl library installed on system
* Rails 4.2
* The user running the application to have access to the git local and remote repository.

## Installing the Migrations

```
bundle exec rake app:bagboy:install:migrations
bundle exec rake db:migrate
```
## Initializer
```ruby
Bagboy.config do |config|
  config.chef_repo      = '/path/to/chef-repo'
  config.site_name      = 'Databags'
  config.scm            = 'git'
  config.key_path       = '/path/to/chef/client.pem'
  config.config_path    = '/path/to/chef/knife.rb'
end
```

