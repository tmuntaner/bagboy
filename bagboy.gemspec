$:.push File.expand_path('../lib', __FILE__)

require 'bagboy/version'

Gem::Specification.new do |s|
  s.name        = 'bagboy'
  s.version     = Bagboy::VERSION
  s.authors     = ['Thomas Muntaner']
  s.email       = ['thomas.muntaner@gmail.com']
  s.homepage    = 'https://github.com/rubyrainbows/bagboy'
  s.summary     = 'Creates data bags for chef.'
  s.description = 'Creates data bags for chef.'
  s.license     = 'MIT'

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.rdoc']
  s.test_files = Dir['test/**/*']

  s.add_dependency 'sass-rails', '~> 5.0'
  s.add_dependency 'rails', '~> 4.2'
  s.add_dependency 'haml-rails', '~> 0.9'
  s.add_dependency 'bcrypt-ruby', '~> 3.1'
  s.add_dependency 'chef', '~> 12.19'
  s.add_dependency 'coffee-rails', '~> 4.2'
  
  s.add_development_dependency 'sqlite3', '~> 1.3'
  s.add_development_dependency 'rspec-rails', '~> 3.5'
  s.add_development_dependency 'capybara', '~> 2.12'
  s.add_development_dependency 'factory_girl_rails', '~> 4.8'
  s.add_development_dependency 'database_cleaner', '1.5'
  s.add_development_dependency 'pry', '~> 0.10'
  s.add_development_dependency 'better_errors', '~> 2.1'
  s.add_development_dependency 'binding_of_caller', '~> 0.7'
  
end
