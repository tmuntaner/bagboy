class CreateBagboyTemplates < ActiveRecord::Migration
  def change
    create_table :bagboy_templates do |t|
      t.text :data
      t.string :data_bag_name
      t.timestamps
    end
  end
end
