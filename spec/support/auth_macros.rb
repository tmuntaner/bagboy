module AuthMacros
  def login
    @_current_user = FactoryGirl.create(:bagboy_user)
    visit bagboy_signin_path
    fill_in 'email', with: @_current_user.email
    fill_in 'password', with: 'foobar'
    click_on 'submit'
  end

  def current_user
    @_current_user
  end
end