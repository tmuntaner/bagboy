require 'spec_helper'

module Bagboy
  module Chef
    describe Knife  do
      
      let(:knife) { Knife.instance }
      let(:file) { double('file') }

      after(:each) do
        Bagboy.config do |config|
          config.config_path  = ''
          config.key_path     = ''
        end
      end

      describe '#update_item' do
        it "should update item" do
          cmd = "cd #{Bagboy.chef_repo}; knife data bag from file foo bar.json"
          expect(knife).to receive(:`).once.with(cmd)
          knife.update_item('foo', 'bar')
        end
      end

      describe '#create_data_bag' do

        it "should create a data bag" do
          cmd = "cd #{Bagboy.chef_repo}; knife data bag create foo"
          expect(knife).to receive(:`).once.with(cmd)
          knife.create_data_bag 'foo'
        end
        
      end

      describe '#knife_command' do

        it 'should use key path and key config if supplied' do
          Bagboy.config do |config|
            config.config_path  = '/foo'
            config.key_path     = '/bar'
          end
          cmd = "cd #{Bagboy.chef_repo}; knife foo -k /bar -c /foo bar"
          expect(knife.knife_command('foo','bar')).to eq(cmd)
        end

        it 'should ignore key path and key config if not supplied' do
          cmd = "cd #{Bagboy.chef_repo}; knife foo bar"
          expect(knife.knife_command('foo','bar')).to eq(cmd)
        end

      end

    end
  end
end
