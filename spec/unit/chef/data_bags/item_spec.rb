require 'spec_helper'

module Bagboy
  module Chef
    module DataBags
      describe Item do

        let(:file) { double('file') }
        let(:path) { '/foo/bar' }
        let(:item) { Item.new( {path: path, file: file} ) }
        let(:data) { {foo: 'bar'} }
        let(:fields) { {password: 'password', array: 'array', text: 'text'} }

        describe '#data' do
          it 'should give data if file exists' do
            expect(JSON).to receive(:parse).and_return(data)
            expect(File).to receive(:exists?).and_return(true)
            expect(file).to receive(:read).with(path).and_return(data)
            expect(item.data).to eq( data )
          end

          it 'should not give data for non-existant files' do
            expect(File).to receive(:exists?).and_return(false)
            expect(item.data).to eq( {} )
          end
        end

        describe '#update' do
          it 'should parse values' do
            expect(file).to receive(:write).once
            json  = item.update({password: 'secret', array: %w(foo bar), text: 'foo', foo: 'bar'}, fields)

            expect(json).to include(text: 'foo')
            expect(json).to include(array: %w(foo bar))
            expect(json).to include(:password)
          end

          it 'should parse and clean' do
            expect(file).to receive(:write).once
            json  = item.update({password: 'secret ', array: ['foo', 'bar',''], text: 'foo '}, fields)

            expect(json).to include(text: 'foo')
            expect(json).to include(array: %w(foo bar))
            expect(json).to include(:password)
          end

          it 'should not update password if you dont change it' do
            expect(file).to receive(:write).twice
            expect(File).to receive(:exists?).twice.and_return(false)
            json  = item.update({'password' => 'secret', array: %w(foo bar), text: 'foo', foo: 'bar'}, fields)
            item.update({password: '', array: %w(foo bar), text: 'foo', foo: 'bar'}, fields)

            expect(item.data['password']).to eq(json[:password])
          end
        end

      end
    end
  end
end