require 'spec_helper'

module Bagboy
  module Chef
    module DataBags
      describe Bag do
        
        let(:file) { double('file') }
        let(:scm) { double('scm') }
        let(:path) { '/foo/bar' }
        let(:knife) { double('knife') }
        let(:bag) { Bag.new( {path: path, file: file, name: 'foo', scm: scm, knife: knife} ) }

        describe '#items' do
          it "should give items" do
            expect(file).to receive(:get_files).with(path).and_return(['foo.json'])
            item          = double('item')
            expected_opts = {path: File.join(path, 'foo.json'), name: 'foo' }
            expect(DataBags::Item).to receive(:new).once.with(expected_opts).and_return(item)
            items = bag.items
            expect(items).to include(item)
          end
        end

        describe '#item' do
          it "should give item" do
            item = double('item')
            expect(DataBags::Item).to receive(:new).once.with({name: 'foo', path: '/foo/bar/foo.json', bag: bag.name}).and_return(item)
            bag.item 'foo'
          end
        end

        describe '#create' do
          it "should create a data bag if unique" do
            expect(Dir).to receive(:mkdir).once
            expect(scm).to receive(:pull).once
            expect(file).to receive(:get_files).once.with('/foo').and_return([])
            expect(knife).to receive(:create_data_bag).once.with("foo")
            bag.create
          end
          
          it "should not create a data bag unless unique" do
            expect(scm).to receive(:pull).once
            expect(file).to receive(:get_files).once.with('/foo').and_return(['foo'])
            bag.create
          end

          it "should not create a bag if the name is blank" do
            bad_bag = Bag.new( {path: path, file: file, name: '', scm: scm, knife: knife} )
            expect(scm).to receive(:pull).once
            bad_bag.create
          end
        end

      end
    end
  end
end