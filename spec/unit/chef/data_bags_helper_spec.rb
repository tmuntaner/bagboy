require 'spec_helper'

module Bagboy
  module Chef
    describe DataBagsHelper  do
      
      let(:data_bag_helper) { DataBagsHelper.instance }
      let(:file) { double('file') }

      before(:each) do
        data_bag_helper.setup ( {file: file} )
      end

      it 'should give all the data bags in the data bags directory' do
        bag = double('bag')
        expect(file).to receive(:get_directories).and_return(['foo'])
        expect(DataBags::Bag).to receive(:new) { bag }
        bags = data_bag_helper.data_bags
        expect(bags).to eq([bag])
      end

      it 'should give a data_bag' do
        bag = double('bag')
        expect(DataBags::Bag).to receive(:new) { bag }
        data_bag_helper.data_bag('users')
      end

    end
  end
end
