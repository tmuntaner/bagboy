require 'spec_helper'

module Bagboy
  module Core
    describe SCMHelper do
      
      let(:scm_helper) { SCMHelper.instance }
      let(:scm) { double('scm') }

      before(:each) do
        scm_helper.setup( { scm: scm } )
      end

      after(:each) do
        reset_scm_helper
      end

      describe "setup" do
        it "should setup the scm helper" do
          tmp = double('scm')
          scm_helper.setup( { scm: tmp } )
          expect(scm_helper.scm).to eq(tmp)
        end
      end

      describe "connect" do
        it "should connect to git" do
          reset_scm_helper
          Bagboy.scm = 'git'
          expect(SCM::Git).to receive(:instance).once
          scm_helper.connect
        end

        it "should connect to none" do
          reset_scm_helper
          Bagboy.scm = 'none'
          expect(SCM::None).to receive(:instance).once
          scm_helper.connect
        end
      end

      describe "pull" do
        it "should pull" do
          expect(scm).to receive(:pull).once
          scm_helper.pull
        end
      end

      describe "commit" do
        it "should commit" do
          expect(scm).to receive(:commit).once.with("foo", "bar")
          scm_helper.commit("foo", "bar")
        end
      end

      private

      def reset_scm_helper
        scm_helper.setup( { scm: nil } )
        Bagboy.scm = 'none'
      end

    end
  end
end
