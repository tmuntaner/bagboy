require 'spec_helper'

module Bagboy
  module Core
    describe FileHelper do
      
      let(:file_helper) { FileHelper.instance }
      let(:path) { File.expand_path File.join( File.dirname(__FILE__), '/../../fixtures/files' ) }

      it "should give all the data bags in the data bags directory" do
        allow(Dir).to receive(:entries).and_return(['..', '.', 'bar.rb','foo.json'])
        files = file_helper.get_files path
        expect(files).to eq(["bar.rb","foo.json"])
      end

      it "should read a file" do
        allow(File).to receive(:read).and_return('foo bar')
        data = file_helper.read path
        expect(data).to eq('foo bar')
      end

      it "should write" do
        expect(File).to receive(:open).with( File.join(path, 'write.bar'), 'w' )
        file_helper.write(File.join(path, 'write.bar'), 'foo_bar')
      end

    end
  end
end
