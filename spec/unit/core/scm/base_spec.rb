require 'spec_helper'

module Bagboy
  module Core
    module SCM
      describe Base do

        let(:scm) { SCM::Base.new }

        it "should not pull" do
          expect{scm.pull}.to raise_error(NotImplementedError)
        end

        it "should not commit" do
          file  = File.join( Bagboy.chef_repo, 'foo.bar' )
          expect{ scm.commit( file, "foo bar") }.to raise_error(NotImplementedError)  
        end
    
      end
    end
  end
end
