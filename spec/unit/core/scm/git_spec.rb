require 'spec_helper'

module Bagboy
  module Core
    module SCM
      describe Git do

        let(:chef_repo) { Bagboy.chef_repo }
        let(:git) { SCM::Git.instance }
        let(:path) { Pathname.new(Bagboy.chef_repo) }

        it "should pull" do
          cmd = "cd #{Bagboy.chef_repo}; git pull"
          expect(git).to receive(:`).once.with(cmd)
          git.pull
        end

        it "should commit" do
          file  = File.join( Bagboy.chef_repo, 'foo.bar' ) 
          cmd   = "cd #{Bagboy.chef_repo}; git add 'foo.bar'; git commit 'foo.bar' -m 'foo bar'; git push"
          expect(git).to receive(:`).once.with(cmd)
          git.commit( file, "foo bar")
        end
    
      end
    end
  end
end
