Bagboy.config do |config|
  config.chef_repo      = File.expand_path File.join( File.dirname(__FILE__), '/../../../fixtures/chef_repo_testing' )
  #config.chef_repo      = '/home/tmuntan1/chef-repo'
  config.site_name      = 'Bagboy'
  config.password_hash  = 'foobar'
end