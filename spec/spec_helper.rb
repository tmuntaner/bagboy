# Code Coverage
require 'simplecov'
require 'coveralls'

ENV["RAILS_ENV"] ||= 'test'

#SimpleCov.formatter = SimpleCov::Formatter::MultiFormatter[
#  SimpleCov::Formatter::HTMLFormatter,
#  Coveralls::SimpleCov::Formatter
#]

SimpleCov.start do
  add_filter '/spec/'
end

require File.expand_path("../dummy/config/environment.rb", __FILE__)
require 'rspec/rails'
require 'capybara/rails'
require 'factory_girl_rails'
require 'pry'
require 'sass'
require 'database_cleaner'
require 'capybara/rspec'

Dir["#{File.dirname(__FILE__)}/support/**/*.rb"].each { |f| require f }

ActiveRecord::Migration.check_pending! if defined?(ActiveRecord::Migration)

Bagboy.config do |config|
  config.chef_repo      = File.expand_path File.join( File.dirname(__FILE__), '/fixtures/chef_repo_testing' )
  config.password_hash  = 'foobar'
end

RSpec.configure do |config|

  config.mock_with :rspec
  config.infer_base_class_for_anonymous_controllers = false
  config.order = 'random'
  config.include FactoryGirl::Syntax::Methods
  config.include AuthMacros
  config.include Capybara::DSL, example_group: { file_path: /\bspec\/integration\// }
  config.include Bagboy::Engine.routes.url_helpers
  config.before(:each, type: :controller) { @routes = Bagboy::Engine.routes }
  config.before(:each, type: :routing)    { @routes = Bagboy::Engine.routes }

  config.before(:each) do
    DatabaseCleaner.strategy = :truncation
    DatabaseCleaner.start
    reset_mocks
    clean_chef_repo
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end

end

def clean_chef_repo
  path = File.join( Bagboy.chef_repo, '/data_bags' )
  `rm -rf #{path}/*`
end

def reset_mocks
  bag_helper  = Bagboy::Chef::DataBagsHelper.instance
  file        = Bagboy::Core::FileHelper.instance
  bag_helper.setup ( {file: file} )
end
