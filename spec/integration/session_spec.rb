require 'spec_helper'

describe 'session' do
  
  it 'should sign in user with proper password' do
    user = FactoryGirl.create(:bagboy_user)
    visit bagboy_signin_path
    fill_in 'email', with: user.email
    fill_in 'password', with: 'foobar'
    click_on 'submit'
    expect(page).to have_content 'You have successfuly logged in!'
  end

  it 'should not sign in user without proper password' do
    user = FactoryGirl.create(:bagboy_user)
    visit bagboy_signin_path
    fill_in 'email', with: user.email
    fill_in 'password', with: 'hunter2'
    click_on 'submit'
    expect(page).not_to have_content 'You have successfuly logged in!'
    expect(page).to have_content 'Invalid email or password'
  end

  it 'should sign out a user' do
    visit root_path
    expect(page).to have_content 'Create User'
    click_link 'Create User'
    fill_in 'user_email', with: 'foo@bar'
    fill_in 'user_password', with: 'foobar'
    fill_in 'user_password_confirmation', with: 'foobar'
    click_on 'submit'
    expect(page).to have_content 'You have succesfully created foo@bar'
  end

  it 'should sign out a user' do
    login
    visit signout_path
    expect(page).to have_content 'You have successfuly logged out!'
  end

end