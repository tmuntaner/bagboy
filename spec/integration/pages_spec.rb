require 'spec_helper'

describe 'pages' do
  
  it 'should have root page' do
    login
    visit root_path
    expect(page).to have_content 'Overview'
  end

  it 'should update' do
    login
    click_on 'chef_repo_update'
    expect(page).to have_content 'You have successfully updated your chef-repo'
  end

  it 'should try to update and notify when something goes wrong' do
    login
    allow(Bagboy::Core::SCMHelper.instance).to receive(:pull) { false }
    click_on 'chef_repo_update'
    expect(page).to_not have_content 'You have successfully updated your chef-repo'
  end

end