FactoryGirl.define do
  factory :bagboy_user, class: Bagboy::User do
    password "foobar"
    password_confirmation "foobar"
    sequence(:email) { |n| "foo#{n}@foo.bar" }
  end
end