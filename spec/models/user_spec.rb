require 'spec_helper'

module Bagboy
  describe User do
    let(:user) { User.create(email: 'foo@bar.com', password: 'secret') }
    
    specify { expect(user).to be_valid }

    describe "email" do

      it "should have unique email addresses" do
        user2 = User.new ( {email: user.email, password: user.password} )
        expect(user2).to_not be_valid
      end

      it "should have an email" do
        user2 = User.new ( {password: user.password} )
        expect(user2).to_not be_valid
      end

    end

    describe "password" do

      it "should have a password" do
        user2 = User.new ( {email: user.email} )
        expect(user2).to_not be_valid
      end

    end
  end
end
