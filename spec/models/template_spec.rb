require 'spec_helper'

module Bagboy
  describe Template do

    let(:template) { Template.create({data: {foo:'bar'}.to_json })}

    it 'should give a basic hash for parsed data if no data' do
      temp = Template.create()
      expect(temp.parsed_data).to eq({'id' => 'text'})
    end

    it 'should give a hash for parsed data' do
      expect(template.parsed_data).to eq({'foo' => 'bar', 'id' => 'text'})
    end

    it 'should allow an item to be set' do
      template.item('bar', 'foo')
      expect(template.parsed_data).to include('bar' => 'foo')
    end

    it 'should sort parsed data' do
      template.item('foo1', 'bar1')
      template.item('foo2', 'bar2')
      expect(template.parsed_data).to eq({'foo' => 'bar','foo1' => 'bar1', 'foo2' => 'bar2', 'id' => 'text'})
    end

    it 'should have a default data item of id with value of text' do
      expect(template.parsed_data['id']).to eq('text')
    end

    it "should be able to delete data" do
      template.delete_item('foo')
      expect(template.parsed_data).to eq({'id' => 'text'})
    end

  end
end
